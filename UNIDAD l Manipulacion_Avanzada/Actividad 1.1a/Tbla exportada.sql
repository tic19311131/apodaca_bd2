-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 05-06-2020 a las 07:11:38
-- Versión del servidor: 10.5.0-MariaDB
-- Versión de PHP: 7.4.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `guerras`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `bandos`
--

CREATE TABLE `bandos` (
  `id_bando` int(11) NOT NULL,
  `nombre` varchar(255) NOT NULL,
  `ganador` varchar(255) NOT NULL,
  `prop_bando` enum('PARTICIPAR','HACER') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `guerra`
--

CREATE TABLE `guerra` (
  `id_guerra` int(11) NOT NULL,
  `nombre` varchar(255) NOT NULL,
  `año_inicio` date NOT NULL,
  `año_fin` int(11) NOT NULL,
  `pro_guerra` enum('HACER') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `paises`
--

CREATE TABLE `paises` (
  `id_pais` int(11) NOT NULL,
  `nombre` varchar(255) NOT NULL,
  `pro_participar` enum('PARTICIPAR') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `periodos_independencias`
--

CREATE TABLE `periodos_independencias` (
  `id_pais` int(11) NOT NULL,
  `inicio_periodo` date NOT NULL,
  `fin_periodo` date NOT NULL,
  `n_periodo` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `bandos`
--
ALTER TABLE `bandos`
  ADD PRIMARY KEY (`id_bando`);

--
-- Indices de la tabla `guerra`
--
ALTER TABLE `guerra`
  ADD PRIMARY KEY (`id_guerra`);

--
-- Indices de la tabla `paises`
--
ALTER TABLE `paises`
  ADD PRIMARY KEY (`id_pais`);

--
-- Indices de la tabla `periodos_independencias`
--
ALTER TABLE `periodos_independencias`
  ADD PRIMARY KEY (`id_pais`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `bandos`
--
ALTER TABLE `bandos`
  MODIFY `id_bando` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `guerra`
--
ALTER TABLE `guerra`
  MODIFY `id_guerra` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `paises`
--
ALTER TABLE `paises`
  MODIFY `id_pais` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `periodos_independencias`
--
ALTER TABLE `periodos_independencias`
  MODIFY `id_pais` int(11) NOT NULL AUTO_INCREMENT;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `bandos`
--
ALTER TABLE `bandos`
  ADD CONSTRAINT `FK_bandos_guerra` FOREIGN KEY (`id_bando`) REFERENCES `guerra` (`id_guerra`),
  ADD CONSTRAINT `FK_bandos_paises` FOREIGN KEY (`id_bando`) REFERENCES `paises` (`id_pais`);

--
-- Filtros para la tabla `paises`
--
ALTER TABLE `paises`
  ADD CONSTRAINT `FK_paises_periodos_independencias` FOREIGN KEY (`id_pais`) REFERENCES `periodos_independencias` (`id_pais`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
